﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusinesss(business);

            Console.ReadLine();
            while (true)
            {

            }

        }

        static void SerializeBusinesss(Business business)
        {
            // Create a binary formatter
            BinaryFormatter formatter = new BinaryFormatter();            

            string fileName = "C:\\ADN\\test.xml";

            // Create a file using the passed-in file name
            // Use a using statement to automatically clean up object references
            // and close the file handle when the serialization process is complete
            using (Stream stream = File.Create(fileName))
            {
                // Serialize (save) the current instance of the zoo
                formatter.Serialize(stream, business);
            }
        }

    }
}
