﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    [Serializable]
    public class Employee
    {
        private decimal hourlyWage;

        private int hoursScheduled;

        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        private Employee() { }

        public decimal Paycheck { get; set; }

        public int HoursScheduled 
        {
            get { return this.hoursScheduled; } 
        }

        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled, and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.

            if (this.hoursScheduled < work.TimeInvestmentRemaining)
            {
                work.TimeInvestmentRemaining -= this.hoursScheduled;
                work.JobCost += this.PaymentAndCost(this.hoursScheduled);
                this.hoursScheduled = 0;
            }
            else
            {
                this.hoursScheduled -= work.TimeInvestmentRemaining;
                work.JobCost += this.PaymentAndCost(work.TimeInvestmentRemaining);
                work.TimeInvestmentRemaining = 0;
            }            
        }

        /// <summary>
        /// Incease paycheck amount by workhour and return amount payment increase by.
        /// </summary>
        /// <param name="workHour">The total work hour.</param>
        /// <returns>The amount pay for the job.</returns>
        private decimal PaymentAndCost(int workHour)
        {
            decimal amountToPay = workHour * this.hourlyWage;
            
            // Incease the paycheck per hour.
            this.Paycheck += amountToPay;
            
            // Job Cost
            amountToPay *= 1.5m;

            return amountToPay;
        }

    }
}
