﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace FileMoverService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog eventLog = new EventLog();
            eventLog.Source = "Application";
            eventLog.WriteEntry("The service was started.", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            EventLog eventLog = new EventLog();
            eventLog.Source = "Application";
            eventLog.WriteEntry("The service was stop.", EventLogEntryType.Information);
        }
    }
}
